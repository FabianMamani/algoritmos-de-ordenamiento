package algoritmos;

public class Insercion {
	private int[] _arreglo;
	private int aux;
	public Insercion(int[] arreglo) {
		_arreglo = arreglo;
		aux =0;
	}

	public void iniciar() {
		for(int i=1; i< _arreglo.length;i++) 
		{
			aux = _arreglo[i];
			for(int j=i-1; j>=0 && _arreglo[j] > aux;j--)
			{
				_arreglo[j+1] = _arreglo[j];
				_arreglo[j]= aux;
			}
		}
		
	}

	public void obtener() {
		System.out.println("[ ");
		for(int i=0; i< _arreglo.length;i++) {
			System.out.print(_arreglo[i] + " ");
		}
		System.out.println(" ]");
	}

}
