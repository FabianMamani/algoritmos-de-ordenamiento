package algoritmos;

public class Burbuja {
	private int[] _arreglo;
	private int aux;
	public Burbuja(int[] arreglo) {
		_arreglo = arreglo;
		aux =0;
	}
	
	public void iniciar() 
	{
		for(int i=0; i< _arreglo.length-1; i++)
		{
			for(int j=i; j< _arreglo.length; j++)
			{
				if(_arreglo[i] > _arreglo[j])
				{
					aux = _arreglo[i];
					_arreglo[i] = _arreglo[j];
					_arreglo[j] = aux;
				}
			}
		}
		
	}
	public void obtener()
	{
		System.out.print("[ ");
		for(int i=0; i<_arreglo.length;i++)
			System.out.print(_arreglo[i]+" ");
		System.out.print("]");
	}


}
