package main;

import algoritmos.Burbuja;
import algoritmos.Insercion;

public class Main {

	public static void main(String[] args) {
		int[] arreglo ={5,8,2,4,3}; 
		
//		//M�todo de Burbujeo.
//		Burbuja _burbuja = new Burbuja(arreglo);
//		_burbuja.iniciar();
//		_burbuja.obtener();
		
		//M�todo de Inserci�n.
		Insercion _insercion = new Insercion(arreglo);
		_insercion.iniciar();
		_insercion.obtener();
	}

}
